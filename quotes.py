import time
import spidev as SPI
import EPD_driver
import datetime
import random
import math
import urllib, json
import textwrap
from subprocess import call

EPD02X13 = 1

bus = 0
device = 0

DELAYTIME = 4

disp = EPD_driver.EPD_driver(spi=SPI.SpiDev(bus, device))

# with 12 font, each character is 7 pixels wide and 12 pixels tall
# with 16 font, each character is 11 pixels wide and 16 pixels tall
# resolution is 250 pixels wide and 122 tall

#init and Clear full screen
print ('------------init and Clear full screen------------')
disp.Dis_Clear_full()

#THIS PART INITIALIZES THE SCREEN AND IS CRUCIAL
disp.Dis_Clear_part()

quotes = ["The road to wisdom? -- Well, it\'s plain and simple to express: Err and err and err again but less and less and less.",
"\"Every great developer you know got there by solving problems they were unqualified to solve until they actually did it.\" - Patrick McKenzie",
"Machines work, people should think.",
"\"Instead of wondering when your next vacation is, maybe you should set up a life you don\'t need to escape from\" - Seth Godin",
"Premature Optimization is the root of all evil."]

data = "Error"
getSuccess = True
jsonSuccess = True
try:
    response = urllib.urlopen("http://eltonhartman.com/quotes.json/")
    try:
        data = json.loads(response.read())
    except:
        jsonSuccess = False
        print("Problem with json")
except:
    getSuccess = False
    print("Problem with get request")
    print(response)

if getSuccess and jsonSuccess:
    quotes = data['quotes']
else:
    if getSuccess is False:
        errorString = "Unable to reach server."
    else:
        errorString = "Problem with json"
    disp.Dis_String(0, 0, errorString,16)
    disp.Dis_String(0, 16, "Falling back to onboard quotes.",16)
    time.sleep(10)

rand = random.randint(0,len(quotes)-1)

#Characters per line
c = 30

print (rand)

lines = textwrap.wrap(quotes[rand], c)

print (lines)

for line in range(len(lines)):
	print ("Text to print:" + lines[line])
	disp.Dis_String(0, 16*line, lines[line],16)

#String
print ('------------END------------')
time.sleep(DELAYTIME)

call("sudo poweroff", shell=True)
